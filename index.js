const express = require("express"); // Tương ứng với import express from 'express'
const mongoose = require("mongoose"); // Tương ứng với import mongoose from 'mongoose'
const fetch = require("node-fetch");
const dotenv = require('dotenv');

dotenv.config();

const app = express();
const cors = require('cors');

const productRouter = require("./src/routes/ProductRouter");
const customerRouter = require("./src/routes/CustomerRouter");
const orderRouter = require("./src/routes/OrderRouter");
const orderDetailRouter = require('./src/routes/OrderDetailRouter');

// khai báo CORS
app.use(cors());

// Khai báo body lấy tiếng Việt
app.use(express.urlencoded({
    extended: true
}))
//Khai báo body dạng JSON
app.use(express.json());

const port = process.env.PORT || 8000;

// Kết nối với MongoDB
async function connectMongoDB() {
    await mongoose.connect(process.env.MONGODB_URI || "mongodb://localhost:27017/shop24h");
}

//Thực thi kết nối
connectMongoDB()
    .then(() => console.log("Connect MongoDB Successfully"))
    .catch(err => console.log(err))

app.get("/", (request, response) => {
    response.json({
        message: "Shop24h API"
    })
})

app.post("/checkcaptcha", (req, res) => {
    // getting site key from client side
    const response_key = req.body["token"];
    // Put secret key here, which we get from google console
    const secret_key = "6LcwaaYfAAAAAFbxiA492ZO3nCWEbkh1dbJNpp-o";
   
    // Hitting POST request to the URL, Google will
    // respond with success or error scenario.
    const url =
  `https://www.google.com/recaptcha/api/siteverify?secret=${secret_key}&response=${response_key}`;
   
    // Making POST request to verify captcha
    fetch(url, {
      method: "post",
    })
      .then((response) => response.json())
      .then((google_response) => {
   
        // google_response is the object return by
        // google as a response
        if (google_response.success == true) {
          //   if captcha is verified
          return res.json({ message: "Successful" });
        } else {
          // if captcha is not verified
          return res.send({ message: "Failed" });
        }
      })
      .catch((error) => {
          // Some error while verify captcha
          return res.json({ error });
      });
});

app.use('/products', productRouter);
app.use('/customers', customerRouter);
app.use('/orders', orderRouter);
app.use('/orderdetail', orderDetailRouter);

app.listen(port, () => {
    console.log(`App listening on port ${port}`)
})